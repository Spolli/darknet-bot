#!/usr/bin/env python

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from urllib.request import urlopen
import logging

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)
url = "https://api.telegram.org/file/bot678416773:AAGaM3gCWfe6PpBP59MUztiSI5pQUGSMZac/"

# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(bot, update):
    """Send a message when the command /start is issued."""
    update.message.reply_text('Ciaoo era da un anno che ti aspettavo... Comunque questo è un bot che riconosce gli oggetti nelle foto che mandi!')


def help(bot, update):
    """Send a message when the command /help is issued."""
    update.message.reply_text('Nessun aiuto.')

def trigger(bot, update):
    update.message.reply_text("Stai zitta cagnolina! ʕ•ᴥ•ʔ")


def echoPhoto(bot, update):
    """Echo the user message."""
    fileID = update.message.photo[-1].file_id
    file = bot.get_file(fileID)
    filePath = file.file_path
    bot.send_photo(chat_id=update.message.chat.id, photo=open(url + filePath, 'rb'))
    #update.message.reply_text(img=urlopen(photo).read())

def echoText(bot, update):
    update.message.reply_text(update.message.text)

def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def main():
    """Start the bot."""
    # Create the EventHandler and pass it your bot's token.
    updater = Updater("678416773:AAGaM3gCWfe6PpBP59MUztiSI5pQUGSMZac")

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))
    dp.add_handler(CommandHandler("fanculo", trigger)

    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler(Filters.photo, echoPhoto))
    dp.add_handler(MessageHandler(Filters.text, echoText))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()